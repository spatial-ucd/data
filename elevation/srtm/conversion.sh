#Fails
gdal_translate -projwin -121.0 40.0 -120.0 39.0 -of GTiff /home/<user>/code/srtm/N39_W122_1arc_v3_bil/n39_w122_1arc_v3.bil /home/<user>/code/srtm/N39_W121_1arc_v3_bil/n39_w121_1arc_v3.tif

#Works but just drops 1 row/column, doesn't shift the half pixel coordinates to whole
gdal_translate -a_srs EPSG:4326 -projwin -121 40 -120 39 -of GTiff -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 /home/<user>/code/srtm/N39_W121_1arc_v3_bil/n39_w121_1arc_v3.bil /home/<user>/code/srtm/N39_W121_1arc_v3_bil/n39_w121_1arc_v3.tif

#gdal_warp http://www.gdal.org/gdalwarp.html
#Bilinear and Average have approx the same results in this case.
gdalwarp -t_srs EPSG:4326 -te -121.0000 39.0000 -120.0000 40.0000 -r bilinear -of GTiff -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -multi /home/<user>/code/srtm/N39_W121_1arc_v3_bil/n39_w121_1arc_v3.bil /home/<user>/code/srtm/N39_W121_1arc_v3_bil/n39_w121_1arc_v3-shift.tif
#An alernative to resampling would be to split the original cells into 4
gdalwarp -t_srs EPSG:4326 -te -121.0000 39.0000 -120.0000 40.0000 -r near -ts 7200 7200 -of GTiff -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -multi /home/<user>/code/srtm/N39_W121_1arc_v3_bil/n39_w121_1arc_v3.bil /home/<user>/code/srtm/N39_W121_1arc_v3_bil/n39_w121_1arc_v3-downscale.tif


#for each zip
# unzip to scratch
# unzip -u $item -d /scratch/vulpes/srtm/
# convert to geotiff, and resample to align with 1 degree lines
# n39_w121_1arc_v3.bil
# split filename
# n/s is the y, w/e is the x, s/w=-
# filename contains mins, +1 for max
gdalwarp -t_srs EPSG:4326 -te minx miny maxx maxy -r bilinear -of GTiff -co COMPRESS=DEFLATE -co PREDICTOR=2 -co ZLEVEL=9 -multi $INPUT $OUTPUT
# delete unzip'd folder from scratch
