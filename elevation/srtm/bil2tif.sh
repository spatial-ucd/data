#!/bin/bash
# Script to convert zipped Bil rasters to Geotiff with compression
# @:sbatch --array=2-10000 -p serial --mail-type=ALL --mail-user=you@ucdavis.edu bil2tif.sh
# 12677 files total, so for above 10,000 uncomment the +10000 and use this
# sbatch --array=1-2677 -p serial --mail-type=ALL --mail-user=you@ucdavis.edu bil2tif.sh
# Get the task id which is the line in the catalog to run
## Next 2 lines for testing only
# SLURM_ARRAY_TASK_ID=1000
# FILE="/scratch/test_bil.zip"

echo $(hostname)

LN=$SLURM_ARRAY_TASK_ID
#When running files past 10,000
LN=$(($LN+10000))
SCRATCH="/scratch/${USER}/srtm/"
SCRATCHT="${SCRATCH}/tif/"
echo mkdir -p $SCRATCHT
mkdir -p $SCRATCHT

# READ the line from the csv into variables
IFS="," read FILE YMIN XMIN YMAX XMAX < <(sed -n "${LN}p" < srtm-batch.csv)

# Strip the quotes off the FILE
echo $FILE
FILE="${FILE%\"}"
FILE="${FILE#\"}"

# Split name and create output names
ZIP=$(basename "$FILE")
BILD=${ZIP/.zip/}
BIL=${ZIP/_bil.zip/.bil}
BIL=$(echo $BIL | tr [:upper:] [:lower:])
TIF=${BIL/.bil/.tif}

# Unzip the bil to scratch
echo unzip -u $FILE -d $SCRATCH$BILD
unzip -u $FILE -d $SCRATCH$BILD

echo gdalwarp -t_srs EPSG:4326 -te $XMIN $YMIN $XMAX $YMAX -r bilinear -of GTiff -co \
	COMPRESS=DEFLATE -co PREDICTOR=2 -co ZLEVEL=9 -multi $SCRATCH$BILD/$BIL $SCRATCHT$TIF
gdalwarp -t_srs EPSG:4326 -te $XMIN $YMIN $XMAX $YMAX -r bilinear -of GTiff -co \
	COMPRESS=DEFLATE -co PREDICTOR=2 -co ZLEVEL=9 -multi $SCRATCH$BILD/$BIL $SCRATCHT$TIF


cp $SCRATCHT$TIF ~/hadoop/library/elevation/srtm/v3tif/
rm -r $SCRATCH$BILD
rm -r $SCRATCHT/$TIF
