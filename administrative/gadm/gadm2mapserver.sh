#!/bin/bash
# Put Latest GADM into Mapserver
# The layered version so each layer can be served separately

# TODO: option to take this from commandline arg
SOURCE="http://biogeo.ucdavis.edu/data/gadm2.7/gadm27_levels.gdb.zip"
ITEM="gadm27_levels.gdb.zip"
USER=`whoami`
WKDIR="/scratch/${USER}/"

#echo $SOURCE $ITEM $USER $WKDIR

# Download
wget ${SOURCE} -O ${WKDIR}${ITEM}
# Unzip
unzip -u ${WKDIR}${ITEM} -d ${WKDIR}gdb/

# convert from gdb to spatialite
# TODO: convert to postgis
GDB=${ITEM/.zip/}
NEW=${ITEM/.gdb.zip/.sqlite}
echo $GDB $NEW
ogr2ogr -f SQLite -skip-failures -nlt PROMOTE_TO_MULTI -dsco SPATIALITE=YES ${WKDIR}${NEW} ${WKDIR}gdb/${GDB}

# TODO: move sqlite db to place where mapserver will use it
echo "Please copy ${WKDIR}${NEW} to location where mapserver expects it"
 
