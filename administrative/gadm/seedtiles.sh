#!/bin/bash
# Generate tiles for the layers available to the web map
# Note, run as sudo for proper permissions
# This will wipe all previous tiles
VARS=("adm0" "adm0-labels" "adm1" "adm1-labels")

# TODO Seed layers at appropriate zoom

for TILE in "${VARS[@]}";
do
    echo $TILE
    time sudo -u www-data mapcache_seed -c /home/vulpes/test/spatialdata/administrative/gadm/gadmmapcache.xml -t $TILE -z 0,8 -M 4,4 -n 2
    #Run with -o if you want to wipe existing tiles?
    #time sudo -u www-data mapcache_seed -c /home/vulpes/test/spatialdata/administrative/gadm/gadmmapcache.xml -t $TILE -z 0,8 -M 4,4 -n 2 -o now
done;
